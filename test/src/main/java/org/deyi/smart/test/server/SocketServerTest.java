package org.deyi.smart.test.server;

import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.comm.messages.receive.ReceiveBatteryInfo;
import org.deyi.smart.comm.messages.receive.ReceiveBatteryLimit;
import org.deyi.smart.comm.messages.receive.ReceiveDeviceID;
import org.deyi.smart.comm.messages.receive.ReceiveStep;
import org.deyi.smart.comm.messages.receive.ReceiveSyncTime;
import org.deyi.smart.comm.messages.send.SendBatteryInfo;
import org.deyi.smart.comm.messages.send.SendBatteryLimit;
import org.deyi.smart.comm.messages.send.SendDeviceID;
import org.deyi.smart.comm.messages.send.SendStep;
import org.deyi.smart.comm.messages.send.SendSyncTime;
import org.deyi.smart.common.common.MessageParser;
import org.deyi.smart.common.message.Message;
import org.deyi.smart.common.utils.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class SocketServerTest {

    public static void main(String args[]) {

        int port = 9090;
        ServerSocket listener = null;

        try {
            listener = new ServerSocket(port);

            List<Message> templates = new ArrayList<>();
            templates.add(new SendBatteryInfo());
            templates.add(new SendBatteryLimit(BaseMessage.EMPTY_BYTE));
            templates.add(new SendDeviceID());
            templates.add(new SendStep(BaseMessage.EMPTY_BYTE));
            templates.add(new SendSyncTime(BaseMessage.EMPTY_BYTE));

            final MessageParser parser = new MessageParser(BaseMessage.HEADER, BaseMessage.TAIL, templates);

            while (true) {
                try {
                    new Processor(listener.accept(), UUID.randomUUID().toString(), parser).start();
                } catch (IOException ex) {
                    log(ex.getMessage());
                }
            }
        } catch (Exception ex) {
            log(ex.getMessage());
        } finally {
            if (listener != null) {
                try {
                    listener.close();
                } catch (IOException ex) {
                    log(ex.getMessage());
                }
            }
        }
    }

    static class Processor extends Thread {

        private Socket socket;
        private String sessionId;
        private MessageParser parser;

        public Processor(Socket socket, String sessionId, MessageParser parser) {
            this.socket = socket;
            this.sessionId = sessionId;
            this.parser = parser;
        }

        public void run() {

            try {

                final List<byte[]> messages = new ArrayList<>();

                int len = 0;
                byte[] buffer = new byte[8192];
                InputStream in = socket.getInputStream();
                OutputStream out = socket.getOutputStream();

                while (true) {
                    len = in.read(buffer);

                    if (len <= 0) {
                        break;
                    }

                    byte[] data = new byte[len];
                    Utils.copy(buffer, data);
                    messages.add(data);

                    log("RX", data);

                    Arrays.fill(buffer, (byte) 0);

                    List<Message> parsedMessages = parser.parse(messages);
                    for (Message receive : parsedMessages) {

                        Message send = null;

                        if (Utils.compare(receive.command(), SendDeviceID.COMMAND)) {

                            String deviceId = "testid";
                            send = new ReceiveDeviceID(deviceId.getBytes());

                        } else if (Utils.compare(receive.command(), SendSyncTime.COMMAND)) {

                            send = new ReceiveSyncTime();

                        } else if (Utils.compare(receive.command(), SendStep.COMMAND)) {

                            SendStep stepinfo = (SendStep) receive;
                            int sendLen = 6 + stepinfo.hours() * 2;
                            List<byte[]> sendData = new ArrayList<>();
                            sendData.add("001111".getBytes());
                            for (int i = 0; i < stepinfo.hours(); i++) {
                                sendData.add(new byte[]{0x01, 0x02});
                            }
                            send = new ReceiveStep(Utils.concat(sendData));

                        } else if (Utils.compare(receive.command(), SendBatteryInfo.COMMAND)) {

                            send = new ReceiveBatteryInfo(new byte[]{0x01, 0x02});

                        } else if (Utils.compare(receive.command(), SendBatteryLimit.COMMAND)) {

                            send = new ReceiveBatteryLimit();
                        }

                        if (send != null) {
                            byte[] sendbuffer = send.pack();
                            out.write(sendbuffer);
                            out.flush();
                            log("TX", sendbuffer);
                        }
                    }
                }

            } catch (IOException ex) {
                log(ex.getMessage());
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                }
                log("Connection with client# " + sessionId + " closed");
            }
        }
    }

    private static void log(String message) {
        System.out.println(message);
    }

    private static void log(String tag, byte[] data) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        System.out.println(sdf.format(new Date()) + tag + " " + Utils.bytesToHex(data));
    }
}
