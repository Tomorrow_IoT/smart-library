package org.deyi.smart.test.common;

import org.deyi.smart.common.common.Logger;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LocalLogger implements Logger {

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void trace(String var1) {
        writeLog(var1);
    }

    @Override
    public void trace(String var1, Object var2) {
        writeLog(MessageFormat.format("{0} {1}", var1, var2));
    }

    @Override
    public void trace(String var1, Object var2, Object var3) {
        writeLog(MessageFormat.format("{0} {1} {2}", var1, var2, var3));
    }

    @Override
    public void trace(String var1, Object[] var2) {
        writeLog(MessageFormat.format(var1, var2));
    }

    @Override
    public void trace(String var1, Throwable var2) {
        writeLog(MessageFormat.format("{0} {1}", var1, var2));
    }

    @Override
    public void debug(String var1) {
        writeLog(var1);
    }

    @Override
    public void debug(String var1, Object var2) {
        writeLog(MessageFormat.format("{0} {1}", var1, var2));
    }

    @Override
    public void debug(String var1, Object var2, Object var3) {
        writeLog(MessageFormat.format("{0} {1} {3}", var1, var2, var3));
    }

    @Override
    public void debug(String var1, Object[] var2) {
        writeLog(MessageFormat.format(var1, var2));
    }

    @Override
    public void debug(String var1, Throwable var2) {
        writeLog(MessageFormat.format("{0} {1}", var1, var2));
    }

    @Override
    public void info(String var1) {
        writeLog(var1);
    }

    @Override
    public void info(String var1, Object var2) {
        writeLog(MessageFormat.format("{0} {1}", var1, var2));
    }

    @Override
    public void info(String var1, Object var2, Object var3) {
        writeLog(MessageFormat.format("{0} {1} {3}", var1, var2, var3));
    }

    @Override
    public void info(String var1, Object[] var2) {
        writeLog(MessageFormat.format(var1, var2));
    }

    @Override
    public void info(String var1, Throwable var2) {
        writeLog(MessageFormat.format("{0} {1}", var1, var2));
    }

    @Override
    public void warn(String var1) {
        writeLog(var1);
    }

    @Override
    public void warn(String var1, Object var2) {
        writeLog(MessageFormat.format("{0} {1}", var1, var2));
    }

    @Override
    public void warn(String var1, Object[] var2) {
        writeLog(MessageFormat.format(var1, var2));
    }

    @Override
    public void warn(String var1, Object var2, Object var3) {
        writeLog(MessageFormat.format("{0} {1} {3}", var1, var2, var3));
    }

    @Override
    public void warn(String var1, Throwable var2) {
        writeLog(MessageFormat.format("{0} {1}", var1, var2));
    }

    @Override
    public void error(String var1) {
        writeLog(var1);
    }

    @Override
    public void error(String var1, Object var2) {
        writeLog(MessageFormat.format("{0} {1}", var1, var2));
    }

    @Override
    public void error(String var1, Object var2, Object var3) {
        writeLog(MessageFormat.format("{0} {1} {3}", var1, var2, var3));
    }

    @Override
    public void error(String var1, Object[] var2) {
        writeLog(MessageFormat.format(var1, var2));
    }

    @Override
    public void error(String var1, Throwable var2) {
        writeLog(MessageFormat.format("{0} {1}", var1, var2));
    }

    private void writeLog(String message) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        System.out.println(sdf.format(new Date()) + ":" + message);
    }

}
