package org.deyi.smart.test.server;

import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.comm.messages.receive.ReceiveBatteryInfo;
import org.deyi.smart.comm.messages.receive.ReceiveBatteryLimit;
import org.deyi.smart.comm.messages.receive.ReceiveDeviceID;
import org.deyi.smart.comm.messages.receive.ReceiveStep;
import org.deyi.smart.comm.messages.receive.ReceiveSyncTime;
import org.deyi.smart.comm.messages.send.SendBatteryInfo;
import org.deyi.smart.comm.messages.send.SendBatteryLimit;
import org.deyi.smart.comm.messages.send.SendDeviceID;
import org.deyi.smart.comm.messages.send.SendStep;
import org.deyi.smart.comm.messages.send.SendSyncTime;
import org.deyi.smart.common.common.Logger;
import org.deyi.smart.common.common.MessageParser;
import org.deyi.smart.common.message.Message;
import org.deyi.smart.common.utils.Utils;
import org.deyi.smart.test.client.SerialClient;
import org.deyi.smart.test.common.LocalLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import gnu.io.SerialPort;

public class SerialServer {

    public static final int TIMEOUTSECONDS = 30;
    public static final int BAUD = 38400;
    public static final String COMM_PORT = "COM7";
    private static SerialClient client = null;

    public static void main(String args[]) {

        Logger logger = new LocalLogger();
        client = new SerialClient(logger);

        try {

            if (client.connect(COMM_PORT, TIMEOUTSECONDS, BAUD, SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1, SerialPort.PARITY_NONE)) {

                List<Message> templates = new ArrayList<>();
                templates.add(new SendBatteryInfo());
                templates.add(new SendBatteryLimit(BaseMessage.EMPTY_BYTE));
                templates.add(new SendDeviceID());
                templates.add(new SendStep(BaseMessage.EMPTY_BYTE));
                templates.add(new SendSyncTime(BaseMessage.EMPTY_BYTE));

                final MessageParser parser = new MessageParser(BaseMessage.HEADER, BaseMessage.TAIL, templates);

                try {
                    new Processor(client, UUID.randomUUID().toString(), parser, logger).run();
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                }
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        } finally {
            client.disconnect();
            client = null;
        }
    }

    static class Processor implements Runnable {

        private SerialClient client;
        private String sessionId;
        private MessageParser parser;
        private Logger logger;

        public Processor(SerialClient client, String sessionId, MessageParser parser, Logger logger) {
            this.client = client;
            this.sessionId = sessionId;
            this.parser = parser;
            this.logger = logger;
        }

        public void run() {

            try {

                final List<byte[]> messages = new ArrayList<>();

                int len = 0;
                byte[] buffer = new byte[8192];

                while (true) {

                    len = client.receive(buffer);

                    if (len < 0) {
                        break;
                    } else if (len == 0) {
                        Arrays.fill(buffer, (byte) 0);
                        Thread.sleep(10);
                        continue;
                    }

                    byte[] data = new byte[len];
                    Utils.copy(buffer, data);
                    messages.add(data);

                    logger.info("RX", Utils.bytesToHex(data));

                    Arrays.fill(buffer, (byte) 0);

                    List<Message> parsedMessages = parser.parse(messages);
                    for (Message receive : parsedMessages) {

                        Message send = null;

                        if (Utils.compare(receive.command(), SendDeviceID.COMMAND)) {

                            String deviceId = "testid";
                            send = new ReceiveDeviceID(deviceId.getBytes());

                        } else if (Utils.compare(receive.command(), SendSyncTime.COMMAND)) {

                            send = new ReceiveSyncTime();

                        } else if (Utils.compare(receive.command(), SendStep.COMMAND)) {

                            SendStep stepinfo = (SendStep) receive;
                            int sendLen = 6 + stepinfo.hours() * 2;
                            List<byte[]> sendData = new ArrayList<>();
                            sendData.add("001111".getBytes());
                            for (int i = 0; i < stepinfo.hours(); i++) {
                                sendData.add(new byte[]{0x01, 0x02});
                            }
                            send = new ReceiveStep(Utils.concat(sendData));

                        } else if (Utils.compare(receive.command(), SendBatteryInfo.COMMAND)) {

                            send = new ReceiveBatteryInfo(new byte[]{0x01, 0x02});

                        } else if (Utils.compare(receive.command(), SendBatteryLimit.COMMAND)) {

                            send = new ReceiveBatteryLimit();
                        }

                        if (send != null) {
                            byte[] sendbuffer = send.pack();
                            client.send(sendbuffer);
                            logger.info("TX", Utils.bytesToHex(sendbuffer));
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            } finally {
                client.disconnect();
                logger.error("Server:" + sessionId + " closed");
            }
        }
    }
}
