package org.deyi.smart.test.client;


import org.deyi.smart.common.common.CommClient;
import org.deyi.smart.common.common.Logger;

import java.io.IOException;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

public class SerialClient extends CommClient {

    private String portName;
    private int baud;
    private int dataBits;
    private int stopBits;
    private int parity;
    private int connectionTimeout;
    private SerialPort serialPort = null;

    public SerialClient(Logger logger) {
        super(logger);
    }

    public boolean connect(String portName, int connectionTimeout,
                           int baud, int dataBits, int stopBits, int parity) {

        try {

            if (isConnected) {
                disconnect();
            }

            CommPortIdentifier commPortIdentifier = CommPortIdentifier.getPortIdentifier(portName);
            int portType = commPortIdentifier.getPortType();

            if (portType != CommPortIdentifier.PORT_SERIAL
                    && portType != CommPortIdentifier.PORT_RS485) {
                isConnected = false;
                return isConnected;
            }

            CommPort commPort = commPortIdentifier.open(this.getClass().getName(), connectionTimeout * 1000);
            serialPort = (SerialPort) commPort;

            serialPort.setSerialPortParams(baud, dataBits, stopBits, parity);

            inputStream = serialPort.getInputStream();
            outputStream = serialPort.getOutputStream();

            this.baud = baud;
            this.dataBits = dataBits;
            this.stopBits = stopBits;
            this.parity = parity;
            this.connectionTimeout = connectionTimeout;

            isConnected = true;

        } catch (Exception ex) {

            if (serialPort != null) {
                serialPort.close();
                serialPort = null;
            }

            isConnected = false;
        }

        return isConnected;
    }

    @Override
    public void disconnect() {

        super.disconnect();

        if (serialPort != null) {
            serialPort.close();
        }
        serialPort = null;
    }

}
