package org.deyi.smart.test.client;

import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.comm.messages.receive.ReceiveBatteryInfo;
import org.deyi.smart.comm.messages.receive.ReceiveBatteryLimit;
import org.deyi.smart.comm.messages.receive.ReceiveDeviceID;
import org.deyi.smart.comm.messages.receive.ReceiveStep;
import org.deyi.smart.comm.messages.receive.ReceiveSyncTime;
import org.deyi.smart.comm.messages.send.SendBatteryInfo;
import org.deyi.smart.comm.messages.send.SendBatteryLimit;
import org.deyi.smart.comm.messages.send.SendDeviceID;
import org.deyi.smart.comm.messages.send.SendStep;
import org.deyi.smart.comm.messages.send.SendSyncTime;
import org.deyi.smart.common.common.Logger;
import org.deyi.smart.common.common.MessageParser;
import org.deyi.smart.common.common.SocketClient;
import org.deyi.smart.common.message.Message;
import org.deyi.smart.common.utils.Utils;
import org.deyi.smart.test.common.LocalLogger;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SocketClientTest {

    static Object obj = new Object();

    public static void main(String args[]) {

        int port = 9090;
        SocketClient client = null;
        Processor processor = null;

        try {

            Logger logger = new LocalLogger();

            client = new SocketClient(logger);
            if (client.connect("127.0.0.1", port)) {
                processor = new Processor(client);
                processor.start();

                Message message = null;

                message = new SendDeviceID();
                client.send(message.pack());

                message = new SendSyncTime("001111".getBytes());
                client.send(message.pack());

                message = new SendStep(new byte[]{0x00});
                client.send(message.pack());

                message = new SendStep(new byte[]{0x01});
                client.send(message.pack());

                message = new SendStep(new byte[]{0x02});
                client.send(message.pack());

                message = new SendStep(new byte[]{0x04});
                client.send(message.pack());

                message = new SendBatteryInfo();
                client.send(message.pack());

                message = new SendBatteryLimit(new byte[]{0x01, 0x02, 0x03, 0x04});
                client.send(message.pack());

                synchronized (obj) {
                    obj.wait();
                }

                client.disconnect();
                client = null;
            }

        } catch (Exception ex) {
            log("error" + ex.getMessage(), new byte[]{});
        } finally {
            if (client != null) {
                client.disconnect();
                client = null;
            }
        }
    }

    static class Processor extends Thread {

        private final List<byte[]> messages = new ArrayList<>();
        private final SocketClient client;
        private final MessageParser parser;

        public Processor(SocketClient client) {
            this.client = client;
            List<Message> templates = new ArrayList<>();
            templates.add(new ReceiveBatteryInfo(BaseMessage.EMPTY_BYTE));
            templates.add(new ReceiveBatteryLimit());
            templates.add(new ReceiveDeviceID(BaseMessage.EMPTY_BYTE));
            templates.add(new ReceiveStep(BaseMessage.EMPTY_BYTE));
            templates.add(new ReceiveSyncTime());
            parser = new MessageParser(BaseMessage.HEADER, BaseMessage.TAIL, templates);
        }

        public void run() {

            byte[] buffer = new byte[8192];
            while (true) {

                int len = client.receive(buffer);
                if (len <= 0) {
                    break;
                }

                byte[] data = new byte[len];
                Utils.copy(buffer, data);

                log("RX", data);

                Arrays.fill(buffer, (byte) 0);
                List<Message> result = parse(data);
                for (Message message : result) {
                    System.out.print(Utils.bytesToHex(message.pack()));
                    if (Utils.compare(message.command(), ReceiveBatteryInfo.COMMAND)) {
                        synchronized (obj) {
                            obj.notifyAll();
                        }
                        break;
                    } else if (Utils.compare(message.command(), ReceiveDeviceID.COMMAND)) {
                        ReceiveDeviceID msg = (ReceiveDeviceID) message;
                        log("DeviceId:" + msg.getDeviceID(), new byte[]{});
                    }
                }
            }
        }

        public List<Message> parse(byte[] data) {

            messages.add(data);
            List<Message> result = parser.parse(messages);

            return result;
        }
    }

    private static void log(String tag, byte[] data) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        System.out.println(sdf.format(new Date()) + tag + " " + Utils.bytesToHex(data));
    }
}
