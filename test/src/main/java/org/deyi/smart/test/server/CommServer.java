package org.deyi.smart.test.server;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Enumeration;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.ParallelPort;
import gnu.io.SerialPort;

public class CommServer {

    /**
     * How long to wait for the open to finish up.
     */
    public static final int TIMEOUTSECONDS = 30;
    /**
     * The baud rate to use.
     */
    public static final int BAUD = 19200;

    public static final String COMM_PORT = "COM6";

    public static void main(String args[]) {

        CommPort thePort = null;
        /** The input stream */
        InputStream is;
        /** The output stream */
        PrintStream os;

        try {

            Enumeration<CommPortIdentifier> portEnum = CommPortIdentifier.getPortIdentifiers();
            while ( portEnum.hasMoreElements() )
            {
                CommPortIdentifier portIdentifier = portEnum.nextElement();
                System.out.println(portIdentifier.getName()  +  " - " +  getPortTypeName(portIdentifier.getPortType()) );
            }

            CommPortIdentifier commPortIdentifier = CommPortIdentifier.getPortIdentifier(COMM_PORT);

            switch (commPortIdentifier.getPortType()) {
                case CommPortIdentifier.PORT_SERIAL:
                    thePort = commPortIdentifier.open("DarwinSys DataComm",TIMEOUTSECONDS * 1000);
                    SerialPort myPort = (SerialPort) thePort;

                    // set up the serial port
                    myPort.setSerialPortParams(BAUD, SerialPort.DATABITS_8,
                            SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

                    myPort.close();

                    break;

                case CommPortIdentifier.PORT_PARALLEL:
                    thePort = commPortIdentifier.open("DarwinSys Printing",
                            TIMEOUTSECONDS * 1000);
                    ParallelPort pPort = (ParallelPort) thePort;

                    // Tell API to pick "best available mode" - can fail!
                    // myPort.setMode(ParallelPort.LPT_MODE_ANY);

                    // Print what the mode is
                    int mode = pPort.getMode();
                    switch (mode) {
                        case ParallelPort.LPT_MODE_ECP:
                            System.out.println("Mode is: ECP");
                            break;
                        case ParallelPort.LPT_MODE_EPP:
                            System.out.println("Mode is: EPP");
                            break;
                        case ParallelPort.LPT_MODE_NIBBLE:
                            System.out.println("Mode is: Nibble Mode.");
                            break;
                        case ParallelPort.LPT_MODE_PS2:
                            System.out.println("Mode is: Byte mode.");
                            break;
                        case ParallelPort.LPT_MODE_SPP:
                            System.out.println("Mode is: Compatibility mode.");
                            break;
                        // ParallelPort.LPT_MODE_ANY is a "set only" mode;
                        // tells the API to pick "best mode"; will report the
                        // actual mode it selected.
                        default:
                            throw new IllegalStateException("Parallel mode " +
                                    mode + " invalid.");
                    }
                    break;
                default:  // Neither parallel nor serial??
                    throw new IllegalStateException("Unknown port type " + COMM_PORT);
            }

            is = thePort.getInputStream();
            os = new PrintStream(thePort.getOutputStream(), true);



        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.print(ex.getMessage());
        }
    }
    static String getPortTypeName ( int portType )
    {
        switch ( portType )
        {
            case CommPortIdentifier.PORT_I2C:
                return "I2C";
            case CommPortIdentifier.PORT_PARALLEL:
                return "Parallel";
            case CommPortIdentifier.PORT_RAW:
                return "Raw";
            case CommPortIdentifier.PORT_RS485:
                return "RS485";
            case CommPortIdentifier.PORT_SERIAL:
                return "Serial";
            default:
                return "unknown type";
        }
    }
}
