package org.deyi.smart.common.utils;

public class RC4Util {

    private final byte[] key = new byte[16];

    private int mi;
    private int mj;
    private byte[] ms = new byte[256];

    private RC4Util() {

    }

    public static RC4Util getInstance(byte[] key) {

        RC4Util instance= new RC4Util();
        instance.init(key);

        return instance;
    }

    private void init(byte[] key) {

        if ( key != null && key.length > 0 ){
            System.arraycopy(key,0,this.key,0,key.length <= 16 ? key.length : 16);
        }

        int j = 0;
        for (int i = 0; i < 256; i++) {
            ms[i] = (byte) i;
        }

        byte temp;
        for (int i = 0; i < 256; i++) {
            j = (j + (ms[i] & 0xFF) + key[i % key.length] & 0xFF) % 256;
            temp = ms[j];
            ms[j] = ms[i];
            ms[i] = temp;
        }
        mi = mi = 0;
    }

    public void reset() {
        mi = 0;
        mj = 0;
        System.arraycopy(new byte[ms.length], 0, ms, 0, ms.length);
        init(this.key);
    }

    public byte[] encodeOrDecode(byte[] data) {

        if (data == null || data.length <= 0) {
            return data;
        }
        int len = data.length;
        byte[] buffer = new byte[len];

        int i = mi;
        int j = mj;

        byte temp;
        for (int y = 0; y < len; y++) {
            i = (i + 1) % 256;
            j = (j + (ms[i] & 0xFF)) % 256;

            temp = ms[i];
            ms[i] = ms[j];
            ms[j] = temp;

            temp = (byte) ((data[y] & 0xFF) ^ (byte) (ms[((ms[i] & 0xFF) + (ms[j] & 0xFF)) % 256]));
            buffer[y] = temp;
        }
        mi = i;
        mj = j;

        return buffer;

    }
}
