package org.deyi.smart.common.model.rest;

public class Response<T> {
    public boolean isError;
    public int statusCode;
    public String errorMessage;
    public T body;
}
