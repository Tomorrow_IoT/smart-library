package org.deyi.smart.common.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.deyi.smart.common.common.Logger;
import org.deyi.smart.common.model.rest.BaseRequestModel;
import org.deyi.smart.common.model.rest.Request;
import org.deyi.smart.common.model.rest.Response;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class RESTUtil {

    public static final String APPLICATION_JSON = "application/json";
    public static final String ENCODE_UTF8 = "utf-8";
    public static final int CONST_REQUEST_RETRY = 3;
    public static final int CONST_REQUEST_TIMEOUT = 15 * 1000;

    private static int BUFFER_SIZE = 8192;

    private static <S extends BaseRequestModel, R> Response<R> executeOnce(
            Request<S> request, Class<R> clazz, Logger logger) {

        HttpURLConnection connection = null;
        Response<R> result = new Response<R>();

        OutputStreamWriter out = null;
        Reader reader = null;

        try {

            result.isError = true;
            URL url = new URL(request.url);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json; charset="
                    + ENCODE_UTF8);
            connection.setRequestProperty("Connection", "close");
            connection.setRequestProperty("Accept", APPLICATION_JSON);
            connection.setRequestProperty("Accept-Charset", ENCODE_UTF8);
            connection.setUseCaches(false);
            connection.setConnectTimeout(CONST_REQUEST_TIMEOUT);
            connection.setReadTimeout(CONST_REQUEST_TIMEOUT);

            out = new OutputStreamWriter(connection.getOutputStream());
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

            StringWriter sw = new StringWriter();
            mapper.writeValue(sw, request.request);
            logger.debug("web api url:[" + request.url + "]");
            logger.debug("web api send data[JSON]:\n" + sw.toString() + "\n");
            out.write(sw.toString());
            out.close();
            out = null;

            Thread.sleep(10);

            result.statusCode = connection.getResponseCode();
            result.errorMessage = connection.getResponseMessage();
            logger.debug("web api result[code]:" + result.statusCode + "[message]:"
                    + result.errorMessage);
            if (result.statusCode != HttpURLConnection.HTTP_OK
                    && result.statusCode != HttpURLConnection.HTTP_NO_CONTENT) {
                return result;
            }

            sw.close();
            long total = 0;
            if (result.statusCode != HttpURLConnection.HTTP_NO_CONTENT) {
                sw = new StringWriter();
                reader = new InputStreamReader(connection.getInputStream(), ENCODE_UTF8);
                char[] buffer = new char[BUFFER_SIZE];
                int len = 0;

                while ((len = reader.read(buffer)) > 0) {
                    sw.write(buffer, 0, len);
                    total += len;
                }
                logger.debug("web api received data[JSON]:\n" + sw.toString() + "\n");
                reader.close();
                reader = null;
            }

            if (total > 0) {
                result.body = (R) mapper.readValue(sw.toString(), clazz);
            }

            result.isError = false;

        } catch (Exception ex) {

            logger.error(ex.getMessage(), ex);

        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                }
                out = null;
            }

            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {
                }
            }

            if (connection != null) {
                connection.disconnect();
            }
        }

        return result;
    }

    public static <S extends BaseRequestModel, R> Response<R> execute(
            Request<S> request, Class<R> clazz, Logger logger) {

        boolean isSendOk = false;

        Response<R> result = null;

        for (int i = 0; i < CONST_REQUEST_RETRY; i++) {
            logger.trace("web api url:[" + request.url + "]"
                    + (i > 0 ? ("\tretry count:" + String.valueOf(i + 1)) : ""));
            result = executeOnce(request, clazz, logger);
            if (!result.isError) {
                break;
            }
        }

        return result;
    }
}
