package org.deyi.smart.common.common;


public interface MessageConverter {

    byte[] from(byte[] data);

    byte[] to(byte[] data);

}
