package org.deyi.smart.common.common;

import org.deyi.smart.common.message.Message;
import org.deyi.smart.common.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MessageParser implements MessageConsts {

    private List<Message> messageTemplate = new ArrayList<>();
    private final byte[] header;
    private final byte[] tail;

    public MessageParser(byte[] header, byte[] tail, List<Message> messages) {

        synchronized (messageTemplate) {

            this.header = new byte[header.length];
            if (tail != null && tail.length > 0) {
                this.tail = new byte[tail.length];
            } else {
                this.tail = new byte[]{};
            }
            Utils.copy(header, this.header);
            Utils.copy(tail, this.tail);

            if (messages == null || messages.size() <= 0) return;

            for (Message message : messages) {
                boolean foundSame = false;
                for (Message msg : messageTemplate) {
                    if (Utils.compare(message.header(), msg.header())
                            && Utils.compare(message.tail(), msg.tail())
                            && Utils.compare(message.command(), msg.command())) {
                        foundSame = true;
                        break;
                    }
                }
                if (!foundSame) {
                    messageTemplate.add(message);
                }
            }
        }
    }

    public void AddTemplate(Message message) {

        synchronized (messageTemplate) {

            if (message == null) return;

            boolean foundSame = false;
            for (Message msg : messageTemplate) {
                if (Utils.compare(message.header(), msg.header())
                        && Utils.compare(message.tail(), msg.tail())
                        && Utils.compare(message.command(), msg.command())) {
                    foundSame = true;
                    break;
                }
            }
            if (!foundSame) {
                messageTemplate.add(message);
            }
        }
    }

    public List<Message> parse(List<byte[]> messages) {

        List<Message> result = new ArrayList<>();

        if (messages.size() <= 0) {
            return result;
        }

        byte[] buffer = null;
        byte[] data = null;

        buffer = Utils.concat(messages);

        int start = -1, end = -1;
        while (buffer.length > 0) {

            start = -1;
            end = -1;

            // Find Frame Header
            for (int i = 0; i < buffer.length; i += 1) {
                if (Utils.compare(this.header, buffer, i)) {
                    start = i;
                    break;
                }
            }

            // Find Frame Tail
            if (this.tail.length > 0) {
                for (int i = start + this.header.length; i <= buffer.length; i += 1) {
                    if (Utils.compare(this.tail, buffer, i)) {
                        end = i;
                        break;
                    }
                }
            } else if (start >= 0) {
                // Find Next Frame Header
                for (int i = start + this.header.length; i <= buffer.length; i += 1) {
                    if (Utils.compare(this.header, buffer, i)) {
                        end = i;
                        break;
                    }
                }
            }

            if (start >= 0 && end > start) {

                int len = end - start - this.header.length;
                data = new byte[end - start - this.header.length];
                Utils.copy(buffer, data, start + this.header.length);

                for (Message template : messageTemplate) {
                    byte[] command = new byte[template.command().length];
                    Utils.copy(data, command);
                    if (Utils.compare(command, template.command())) {
                        int bodyLen = len - template.command().length;
                        byte[] body = {};
                        if (bodyLen > 0) {
                            body = new byte[bodyLen];
                            Utils.copy(data, body, template.command().length);
                        }
                        Message message = template.createMessage(body);
                        message.parse();
                        result.add(message);
                        break;
                    }
                }

                int leftlen = (buffer.length - (end + this.tail.length));
                byte[] newBuffer = {};
                messages.clear();
                if (leftlen > 0) {
                    newBuffer = new byte[leftlen];
                    Utils.copy(buffer, newBuffer, end + this.tail.length);
                    messages.add(buffer);
                }
                buffer = newBuffer;
            } else {

                messages.clear();
                if (buffer.length > 0) {
                    messages.add(buffer);
                }

                break;
            }
        }

        return result;
    }
}