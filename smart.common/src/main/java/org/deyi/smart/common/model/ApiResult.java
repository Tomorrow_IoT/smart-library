package org.deyi.smart.common.model;

public class ApiResult<R> {
    public Boolean result;
    public R data;

    public ApiResult(Boolean result, R data) {
        this.result = result;
        this.data = data;
    }
}
