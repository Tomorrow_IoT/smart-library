package org.deyi.smart.common.model.rest;

public class SendLogModel extends BaseRequestModel {
    public String title;
    public String toAddress;
    public String body;
    public byte[] attachFile;
    public String extensionName;
}
