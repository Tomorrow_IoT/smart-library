package org.deyi.smart.common.common;

import java.text.SimpleDateFormat;

public class Consts {

    public static final String SETTING_KEY_SERIAL_NUMBER = "SERIAL_NUMBER";
    public static final String SETTING_KEY_DEBUG_MODE = "DEBUG_MODE";
    public static final String SETTING_KEY_ORIENTATION = "SCREEN_ORIENTATION";

    public static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static final String ERROR_CODE_OK = "0";
}
