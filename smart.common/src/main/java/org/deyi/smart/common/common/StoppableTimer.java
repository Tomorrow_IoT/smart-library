package org.deyi.smart.common.common;

import java.util.Timer;
import java.util.TimerTask;

public class StoppableTimer extends Timer {

    private final Logger logger;
    private StoppableTask task = null;
    private String name;

    public StoppableTimer(String name, Runnable task, long delay, long period, Logger logger) {
        this.logger = logger;
        if (task != null) {
            this.name = name;
            this.task = new StoppableTask(task);
            super.scheduleAtFixedRate(this.task, delay, period);
        }
    }

    public StoppableTimer(String name, Runnable task, long delay, Logger logger) {
        this.logger = logger;
        if (task != null) {
            this.name = name;
            this.task = new StoppableTask(task);
            super.schedule(this.task, delay);
        }
    }

    @Override
    public void cancel() {
        if (this.task != null) {
            this.task.Stop();
            logger.debug("timer:[" + this.name + "] is stopped!");
        }
        super.cancel();
    }

    class StoppableTask extends TimerTask {
        private final Runnable task;
        private volatile Boolean isCanceled = false;

        public StoppableTask(Runnable task) {
            this.task = task;
        }

        public void Stop() {
            this.isCanceled = true;
        }

        @Override
        public void run() {
            if (!this.isCanceled) {
                task.run();
            }
        }
    }

}
