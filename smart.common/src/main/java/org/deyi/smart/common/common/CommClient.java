package org.deyi.smart.common.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Executors;

public class CommClient {

    private static final String TAG = "CommClient";
    private static final Object writeLock = new Object();
    protected final Logger logger;
    protected InputStream inputStream;
    protected OutputStream outputStream;
    protected Boolean isConnected = false;
    private OnCommonErrorListener onCommonErrorListener = null;

    public CommClient(Logger logger) {
        this.logger = logger;
    }

    public CommClient(Logger logger, InputStream inputStream, OutputStream outputStream) {
        this(logger);
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        if (this.inputStream != null && this.outputStream != null) {
            this.isConnected = true;
        }
    }

    public void setOnSocketErrorListener(OnCommonErrorListener onCommonErrorListener) {
        this.onCommonErrorListener = onCommonErrorListener;
    }

    public void disconnect() {

        if (!isConnected) {
            return;
        }

        isConnected = false;

        try {
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException ex) {
            logger.error("Disconnect from server error close input :" + ex.getMessage());
        } finally {
            inputStream = null;
        }

        try {
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException ex) {
            logger.error("Disconnect from server error close output :" + ex.getMessage());
        } finally {
            outputStream = null;
        }

    }

    public Boolean send(byte[] data) {

        boolean result = false;

        synchronized (writeLock) {
            try {
                if (outputStream != null) {
                    outputStream.write(data);
                    outputStream.flush();
                    result = true;
                } else {
                    raiseSocketError(new Exception("socket error,outputStream is null"));
                }
            } catch (Exception ex) {
                logger.error("send error :" + ex.getMessage());
                raiseSocketError(ex);
            }
        }

        return result;
    }

    public Boolean checkConnected() {
        return this.isConnected;
    }

    public int receive(byte buffer[]) {

        int result = -1;

        try {
            if (inputStream != null) {
                result = inputStream.read(buffer);
                if (result > 0) {
                    logger.debug("receive length:" + String.valueOf(result));
                }
                if (result <= 0) {
                    raiseSocketError(new Exception("socket receive error,length:" + String.valueOf(result)));
                }
            } else {
                raiseSocketError(new Exception("socket input is null"));
            }

        } catch (Exception ex) {
            logger.error("receive error :" + ex.getMessage());
            raiseSocketError(ex);
        }

        return result;
    }

    private synchronized final void raiseSocketError(final Exception ex) {

        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                if (onCommonErrorListener != null) {
                    onCommonErrorListener.onError(ex);
                }
            }
        });
    }
}
