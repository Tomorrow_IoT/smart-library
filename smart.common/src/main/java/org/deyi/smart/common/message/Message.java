package org.deyi.smart.common.message;

import org.deyi.smart.common.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class Message {

    protected final byte[] header;
    protected final byte[] tail;
    protected final byte[] command;
    protected final List<byte[]> data = new ArrayList<>();

    public static final Message Empty;
    public static final byte[] EMPTY_BYTE = new byte[]{};

    static {
        Empty = new Message(EMPTY_BYTE, EMPTY_BYTE, EMPTY_BYTE);
    }

    public Message(byte[] header, byte[] tail, byte[] command) {
        this.header = header;
        this.tail = tail;
        this.command = command;
    }

    public Message(byte[] header, byte[] tail, byte[] command, List<byte[]> data) {
        this(header, tail, command);
        if (data != null && data.size() > 0) {
            this.data.clear();
            this.data.addAll(data);
        }
    }

    public Message(byte[] header, byte[] tail, byte[] command, byte[] data) {
        this(header, tail, command);
        if (data.length > 0) {
            this.data.add(data);
        }
    }

    public byte[] header() {
        return this.header;
    }

    public byte[] tail() {
        return this.tail;
    }

    public byte[] command() {
        return this.command;
    }

    public List<byte[]> data() {
        return this.data;
    }

    public void parse() {

    }

    public byte[] pack() {

        List<byte[]> buffer = new ArrayList<>();

        if (this.header != null && this.header.length > 0) {
            buffer.add(this.header);
        }

        if (this.command != null && this.command.length > 0) {
            buffer.add(this.command);
        }

        if (this.data != null && this.data.size() > 0) {
            buffer.addAll(this.data);
        }

        if (this.tail != null && this.tail.length > 0) {
            buffer.add(this.tail);
        }

        byte[] result = Utils.concat(buffer);

        return result;
    }

    public Message createMessage(byte[] data) {
        Message message = new Message(this.header, this.tail, this.command, data);
        return message;
    }
}
