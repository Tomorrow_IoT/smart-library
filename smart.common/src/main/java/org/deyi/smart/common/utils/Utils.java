package org.deyi.smart.common.utils;

import java.util.Arrays;
import java.util.List;

public class Utils {

    final protected static char[] hexArray = "0123456789abcdef".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        char[] hexChars = new char[2];
        int count = 0;
        for (int j = 0; j < bytes.length; j++) {
            count++;
            int v = bytes[j] & 0xFF;
            hexChars[0] = hexArray[v >>> 4];
            hexChars[1] = hexArray[v & 0x0F];
            sb.append(hexChars);

            if (count == 4) {
                sb.append(" ");
                count = 0;
            }
        }
        return sb.toString();
    }

    public static boolean isNullOrEmpty(String str) {

        if (str == null || str.length() <= 0) {
            return true;
        }

        return false;
    }

    public static byte[] concat(List<byte[]> messages) {

        int maxLength = 0;
        for (byte[] message : messages) {
            maxLength += message.length;
        }

        int pos = 0;
        byte[] buffer = new byte[maxLength];
        for (byte[] message : messages) {
            System.arraycopy(message, 0, buffer, pos, message.length);
            pos += message.length;
        }

        return buffer;
    }

    public static void copy(byte[] src, byte[] dest) {
        if (src == null || src.length <= 0 || dest == null || dest.length <= 0) {
            return;
        }
        int len = src.length < dest.length ? src.length : dest.length;
        System.arraycopy(src, 0, dest, 0, len);
    }

    public static void copy(byte[] src, byte[] dest, int start) {

        if (src == null || src.length <= 0 || dest == null || dest.length <= 0) {
            return;
        }
        int len = src.length < dest.length ? src.length : dest.length;
        if ((start + len) > src.length) {
            return;
        }
        System.arraycopy(src, start, dest, 0, len);

    }

    public static boolean compare(final byte[] target, final byte[] data) {

        if (target != null && data != null) {
            if (target.length > data.length) {
                return false;
            } else if (target.length == data.length) {
                if (Arrays.equals(target, data) || target == data) {
                    return true;
                } else {
                    return false;
                }
            } else {
                int len = target.length;
                for (int i = 0; i < len; i++) {
                    if (target[i] != data[i]) {
                        return false;
                    }
                }
                return true;
            }
        }

        return false;
    }

    public static boolean compare(final byte[] target, final byte[] data, final int start) {

        if (start == 0) {
            return compare(target, data);
        }

        if (target != null && data != null) {
            if ((target.length + start) > data.length) {
                return false;
            } else {
                int len = target.length;
                for (int i = 0; i < len; i++) {
                    if (target[i] != data[i + start]) {
                        return false;
                    }
                }
                return true;
            }
        }

        return false;
    }
}
