package org.deyi.smart.common.model.rest;

public class Request<T> {
    public T request;
    public String url;

    public Request(T request) {
        this.request = request;
    }
}
