package org.deyi.smart.common.common;

import org.deyi.smart.common.model.ApiResult;

public interface ApiCallback<R> {
    void onCompleted(ApiResult<R> result);
}
