package org.deyi.smart.common.common;

import java.nio.charset.Charset;

public interface MessageConsts {

    public static final Charset ENCODE = Charset.forName("US-ASCII");
    public static final Charset ENCODE_SJIS = Charset.forName("Shift_JIS");
    public static final Charset ENCODE_UTF8 = Charset.forName("UTF-8");
    public final byte[] ZERO = new byte[]{0};
}
