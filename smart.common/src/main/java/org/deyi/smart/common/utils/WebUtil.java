package org.deyi.smart.common.utils;

import org.deyi.smart.common.common.ApiCallback;
import org.deyi.smart.common.common.Logger;
import org.deyi.smart.common.model.ApiResult;
import org.deyi.smart.common.model.rest.BaseRequestModel;
import org.deyi.smart.common.model.rest.Request;
import org.deyi.smart.common.model.rest.Response;
import org.deyi.smart.common.model.rest.SendLogModel;

public class WebUtil {

    public static final int CONST_REQUEST_RETRY = 3;
    public static final int CONST_REQUEST_TIMEOUT = 15 * 1000;

    // TODO Send Log
    public static final String API_URL = "https://{0}/{1}";

    private static <T extends BaseRequestModel> void prepare(
            Request<T> request, String method) {

    }

    public static ApiResult<Void> sendLog(Request<SendLogModel> request,
                                          ApiCallback<Void> callback, Logger logger) {

        // TODO
        // request.url = MessageFormat.format(SEND_LOG_URL, AgentPanelApplication.getActivateHost());
        Response<Void> result = null;
        for (int i = 0; i < CONST_REQUEST_RETRY; i++) {
            result = RESTUtil.execute(request, Void.class, logger);
            if (!result.isError) {
                break;
            }
        }
        ApiResult<Void> apiResult = new ApiResult<Void>(!result.isError, result.body);
        if (callback != null) {
            callback.onCompleted(apiResult);
        }
        return apiResult;
    }

}
