package org.deyi.smart.common.common;


import org.deyi.smart.common.message.Message;
import org.deyi.smart.common.utils.Utils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


public class MessageReader implements Runnable {

    private final Logger logger;
    private final int BUFFER_SIZE = 8192;
    private final byte[] buffer = new byte[BUFFER_SIZE];
    private final CommClient client;
    private final SynchronousQueue<Message> queue = new SynchronousQueue<>();
    private final List<byte[]> messages = new ArrayList<>();
    private final MessageConverter converter;
    private final MessageParser parser;
    private volatile Boolean isStoped = false;
    private Thread thread;

    public MessageReader(CommClient client, MessageConverter converter,
                         MessageParser messageParser, Logger logger) {

        this.client = client;
        this.converter = converter;
        this.parser = messageParser;
        this.logger = logger;
        thread = new Thread(this);
        thread.start();
    }

    public void stop() {
        this.isStoped = true;
    }

    @Override
    public void run() {

        try {

            int len = 0;

            while (!isStoped) {

                // データ受信
                len = client.receive(buffer);
                if (len <= 0) {
                    queue.put(Message.Empty);
                    return;
                }

                byte[] data = new byte[len];
                System.arraycopy(buffer, 0, data, 0, len);

                if (this.converter != null) {
                    data = this.converter.from(data);
                }

                logger.trace(MessageFormat.format("received data [Hex]←[{0}]",
                        Utils.bytesToHex(data)));

                messages.add(data);
                while (true) {
                    List<Message> list = this.parser.parse(messages);
                    if (list.size() > 0) {
                        queue.putAll(list);
                    } else {
                        break;
                    }
                }

                resetBuffer(buffer);
            }
        } catch (Exception ex) {

            if (ex != null && !Utils.isNullOrEmpty(ex.getMessage())) {
                logger.error(ex.getMessage());
            }
        }

        thread = null;
    }

    private void resetBuffer(byte[] buffer) {
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] = 0;
        }
    }

    public final Message readMessage() {

        Message message = null;
        try {
            message = (Message) queue.take();
        } catch (InterruptedException ex) {
        }

        if (message == null) {
            message = Message.Empty;
        }

        return message;
    }

    public final void release() {

        queue.clear();

        if (thread != null) {
            try {
                thread.interrupt();
            } catch (Exception ex) {
                thread = null;
            }
        }
    }

}
