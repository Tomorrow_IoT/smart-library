package org.deyi.smart.common.common;

import java.util.ArrayList;
import java.util.List;

public class SynchronousQueue<R> {

    private final List<R> queue = new ArrayList<R>();

    public SynchronousQueue() {
    }

    public R take() throws InterruptedException {

        R r = null;

        synchronized (queue) {

            while (queue.size() <= 0) {
                queue.wait();
            }

            if (queue.size() > 0) {
                r = queue.remove(0);
            }
        }

        return r;
    }

    public void put(R item) {

        synchronized (queue) {
            queue.add(item);
            if (queue.size() > 0) {
                queue.notifyAll();
            }
        }
    }

    public void putAll(List<R> items) {

        synchronized (queue) {
            if (items.size() > 0) {
                queue.addAll(items);
            }
            if (queue.size() > 0) {
                queue.notifyAll();
            }
        }
    }

    public void clear() {

        synchronized (queue) {
            queue.clear();
            queue.notifyAll();
        }

    }
}
