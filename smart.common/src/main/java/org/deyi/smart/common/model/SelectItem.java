package org.deyi.smart.common.model;

import org.deyi.smart.common.utils.Utils;

public class SelectItem {

    public String id;
    public String name;

    public SelectItem(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {

        String str = "";
        if (!Utils.isNullOrEmpty(this.name)) {
            str = this.name;
        }

        return str;
    }

}
