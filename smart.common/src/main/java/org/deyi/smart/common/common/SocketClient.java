package org.deyi.smart.common.common;

import java.io.IOException;
import java.net.Socket;

public class SocketClient extends CommClient {

    private String host;
    private int port = -1;
    private Socket socket = null;
    private static final int BUFFER_SIZE = 8192;

    public SocketClient(Logger logger) {
        super(logger);
    }

    public boolean connect(String host, int port) {

        try {

            if (isConnected) {
                disconnect();
            }

            socket = new Socket(host, port);
            socket.setKeepAlive(true);
            socket.setReceiveBufferSize(BUFFER_SIZE);
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            this.host = host;
            this.port = port;

            isConnected = true;

        } catch (Exception ex) {
            logger.error("Connect to server error:" + ex.getMessage());
            isConnected = false;
        }

        return isConnected;
    }

    @Override
    public void disconnect() {

        super.disconnect();

        if (socket != null && !socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException ex) {
                logger.error("Disconnect from server error close socket :" + ex.getMessage());
            }
        }
        socket = null;
    }

}
