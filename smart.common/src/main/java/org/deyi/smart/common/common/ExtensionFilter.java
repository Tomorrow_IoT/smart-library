package org.deyi.smart.common.common;

import java.io.File;
import java.io.FilenameFilter;

public class ExtensionFilter implements FilenameFilter {
    private String suffix = null;

    public ExtensionFilter(final String suffix) {
        this.suffix = suffix;
    }

    public final boolean accept(final File dir, final String name) {
        return name.endsWith(this.suffix);
    }
}
