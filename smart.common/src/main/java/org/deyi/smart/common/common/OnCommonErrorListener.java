package org.deyi.smart.common.common;

public interface OnCommonErrorListener {

    void onError(Exception ex);

}
