package org.deyi.smart.comm.messages.send;


import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.common.message.Message;
import org.deyi.smart.common.utils.Utils;

public class SendStep extends BaseMessage {

    public static final byte[] COMMAND = {0x03};

    public SendStep(byte[] hours) {
        super(COMMAND, hours);
    }

    public int hours() {

        int hour = 0;

        byte[] data = Utils.concat(this.data);
        if (data.length > 0) {
            hour = (int) data[0];
        }

        return hour;
    }

    public SendStep(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(BaseMessage.HEADER, BaseMessage.TAIL, SendStep.COMMAND, data);
    }

    @Override
    public Message createMessage(byte[] data) {
        Message message = new SendStep(BaseMessage.HEADER, BaseMessage.TAIL, SendStep.COMMAND, data);
        return message;
    }
}
