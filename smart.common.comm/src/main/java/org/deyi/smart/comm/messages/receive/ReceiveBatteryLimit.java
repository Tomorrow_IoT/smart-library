package org.deyi.smart.comm.messages.receive;


import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.common.message.Message;

public class ReceiveBatteryLimit extends BaseMessage {

    public static final byte[] COMMAND = {0x0A};

    public ReceiveBatteryLimit() {
        super(COMMAND, EMPTY_BYTE);
    }

    public ReceiveBatteryLimit(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(BaseMessage.HEADER, BaseMessage.TAIL, ReceiveBatteryInfo.COMMAND, data);
    }

    @Override
    public Message createMessage(byte[] data) {
        Message message = new ReceiveBatteryLimit(BaseMessage.HEADER, BaseMessage.TAIL, ReceiveBatteryLimit.COMMAND, data);
        return message;
    }
}
