package org.deyi.smart.comm.messages.send;

import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.common.message.Message;

public class SendDeviceID extends BaseMessage {

    public static final byte[] COMMAND = {0x01};

    public SendDeviceID() {
        super(COMMAND, EMPTY_BYTE);
    }

    public SendDeviceID(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(BaseMessage.HEADER, BaseMessage.TAIL, SendDeviceID.COMMAND, data);
    }

    @Override
    public Message createMessage(byte[] data) {
        Message message = new SendDeviceID(BaseMessage.HEADER, BaseMessage.TAIL, SendDeviceID.COMMAND, data);
        return message;
    }
}
