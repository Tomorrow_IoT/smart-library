package org.deyi.smart.comm.messages.receive;

import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.common.message.Message;

public class ReceiveSyncTime extends BaseMessage {

    public static final byte[] COMMAND = {0x06};

    public ReceiveSyncTime() {
        super(COMMAND, EMPTY_BYTE);
    }

    public ReceiveSyncTime(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(BaseMessage.HEADER, BaseMessage.TAIL, ReceiveSyncTime.COMMAND, data);
    }

    @Override
    public Message createMessage(byte[] data) {
        Message message = new ReceiveSyncTime(BaseMessage.HEADER, BaseMessage.TAIL, ReceiveSyncTime.COMMAND, data);
        return message;
    }

}
