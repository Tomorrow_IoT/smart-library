package org.deyi.smart.comm.messages.receive;

import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.common.message.Message;
import org.deyi.smart.common.utils.Utils;

public class ReceiveDeviceID extends BaseMessage {

    public static final byte[] COMMAND = {0x02};

    private String deviceID = "";

    public ReceiveDeviceID(byte[] data) {
        super(COMMAND, data);
    }

    public String getDeviceID() {
        return this.deviceID;
    }

    @Override
    public void parse() {

        if (this.data.size() > 0) {
            byte[] buffer = Utils.concat(this.data);
            if (buffer.length > 0) {
                this.deviceID = new String(buffer);
            }
        }
    }

    public ReceiveDeviceID(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(BaseMessage.HEADER, BaseMessage.TAIL, ReceiveDeviceID.COMMAND, data);
    }

    @Override
    public Message createMessage(byte[] data) {
        Message message = new ReceiveDeviceID(BaseMessage.HEADER, BaseMessage.TAIL, ReceiveDeviceID.COMMAND, data);
        return message;
    }
}
