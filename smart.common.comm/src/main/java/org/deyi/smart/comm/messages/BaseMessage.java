package org.deyi.smart.comm.messages;

import org.deyi.smart.common.message.Message;

import java.util.List;

public class BaseMessage extends Message {

    public final static byte[] HEADER = {0x55, (byte) 0xAA};
    public final static byte[] TAIL = {0x0D, (byte) 0xCC};

    public final static int COMMAND_LEN = 1;

    protected BaseMessage(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(header, tail, command, data);
    }

    public BaseMessage(byte command, List<byte[]> data) {
        super(HEADER, TAIL, new byte[]{command});
        if (data != null && data.size() > 0) {
            this.data.clear();
            this.data.addAll(data);
        }
    }

    public BaseMessage(byte command, byte[] data) {
        super(HEADER, TAIL, new byte[]{command});
        if (data != null && data.length > 0) {
            this.data.clear();
            this.data.add(data);
        }
    }

    public BaseMessage(byte[] command, List<byte[]> data) {
        super(HEADER, TAIL, command);
        if (data != null && data.size() > 0) {
            this.data.clear();
            this.data.addAll(data);
        }
    }

    public BaseMessage(byte[] command, byte[] data) {
        super(HEADER, TAIL, command);
        if (data != null && data.length > 0) {
            this.data.clear();
            this.data.add(data);
        }
    }
}
