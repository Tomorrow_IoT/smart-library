package org.deyi.smart.comm.messages.receive;

import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.common.message.Message;
import org.deyi.smart.common.utils.Utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ReceiveBatteryInfo extends BaseMessage {

    public static final byte[] COMMAND = {0x08};

    private int batteryInfo = -1;



    public ReceiveBatteryInfo(byte[] data) {
        super(COMMAND, data);
    }

    public int getBatteryInfo() {
        return this.batteryInfo;
    }

    @Override
    public void parse() {

        if (this.data.size() > 0) {
            byte[] bytes = Utils.concat(this.data);
            if (bytes.length > 0) {
                ByteBuffer buffer = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).put(bytes);
                batteryInfo = buffer.getShort(0);
            }
        }
    }

    public ReceiveBatteryInfo(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(BaseMessage.HEADER, BaseMessage.TAIL, ReceiveBatteryInfo.COMMAND, data);
    }

    @Override
    public Message createMessage(byte[] data) {
        Message message = new ReceiveBatteryInfo(BaseMessage.HEADER, BaseMessage.TAIL, ReceiveBatteryInfo.COMMAND,data);
        return message;
    }
}
