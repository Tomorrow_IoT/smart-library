package org.deyi.smart.comm.messages.send;


import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.common.message.Message;
import org.deyi.smart.common.utils.Utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class SendBatteryLimit extends BaseMessage {

    public static final byte[] COMMAND = {0x09};

    private int min;
    private int max;

    public int getMin() {
        return this.min;
    }

    public int getMax() {
        return this.max;
    }

    public SendBatteryLimit(byte[] data) {
        super(COMMAND, data);
        if (data != null && data.length >= 4) {
            byte[] buffer = new byte[2];
            Utils.copy(data, buffer);
            ByteBuffer value = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).put(buffer);
            this.min = value.getShort(0);
            Utils.copy(data, buffer, 2);
            value = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).put(buffer);
            this.max = value.getShort(0);
        }
    }

    public SendBatteryLimit(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(BaseMessage.HEADER, BaseMessage.TAIL, SendBatteryLimit.COMMAND, data);
    }

    @Override
    public Message createMessage(byte[] data) {
        Message message = new SendBatteryLimit(BaseMessage.HEADER, BaseMessage.TAIL, SendBatteryLimit.COMMAND, data);
        return message;
    }
}
