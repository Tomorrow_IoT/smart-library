package org.deyi.smart.comm.messages.send;


import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.common.message.Message;

public class SendBatteryInfo extends BaseMessage {

    public static final byte[] COMMAND = {0x07};

    public SendBatteryInfo() {
        super(COMMAND, EMPTY_BYTE);
    }

    public SendBatteryInfo(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(BaseMessage.HEADER, BaseMessage.TAIL, SendBatteryInfo.COMMAND, data);
    }

    @Override
    public Message createMessage(byte[] data) {
        Message message = new SendBatteryInfo(BaseMessage.HEADER, BaseMessage.TAIL, SendBatteryInfo.COMMAND, data);
        return message;
    }
}
