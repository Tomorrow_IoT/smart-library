package org.deyi.smart.comm.messages.receive;

import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.common.message.Message;
import org.deyi.smart.common.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ReceiveStep extends BaseMessage {

    public static final byte[] COMMAND = {0x04};

    private byte[] lastTime = new byte[6];
    private List<byte[]> steps = new ArrayList<byte[]>();

    public ReceiveStep(byte[] data) {
        super(COMMAND, data);
    }

    public byte[] getLastTime() {
        return this.lastTime;
    }

    public List<byte[]> getSteps() {
        return this.steps;
    }

    @Override
    public void parse() {

        if (this.data.size() > 0) {
            byte[] buffer = Utils.concat(this.data);
            if (buffer.length > 6) {
                Utils.copy(buffer, this.lastTime);
                int len = buffer.length - 6;
                for (int i = 0; i < len; i += 2) {
                    byte[] step = new byte[2];
                    Utils.copy(buffer, step, 6 + i * 2);
                    steps.add(step);
                }
            }
        }
    }

    public ReceiveStep(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(BaseMessage.HEADER, BaseMessage.TAIL, ReceiveStep.COMMAND, data);
    }

    @Override
    public Message createMessage(byte[] data) {
        Message message = new ReceiveStep(BaseMessage.HEADER, BaseMessage.TAIL, ReceiveStep.COMMAND, data);
        return message;
    }
}
