package org.deyi.smart.comm.messages.send;

import org.deyi.smart.comm.messages.BaseMessage;
import org.deyi.smart.common.message.Message;

public class SendSyncTime extends BaseMessage {

    public static final byte[] COMMAND = {0x05};

    public SendSyncTime(byte[] time) {
        super(COMMAND, time);
    }

    public SendSyncTime(byte[] header, byte[] tail, byte[] command, byte[] data) {
        super(BaseMessage.HEADER, BaseMessage.TAIL, SendSyncTime.COMMAND, data);
    }

    @Override
    public Message createMessage(byte[] data) {
        Message message = new SendSyncTime(BaseMessage.HEADER, BaseMessage.TAIL, SendSyncTime.COMMAND, data);
        return message;
    }
}