package org.deyi.smart.common.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import org.deyi.smart.common.R;
import org.deyi.smart.common.common.Consts;
import org.deyi.smart.common.common.Logger;
import org.deyi.smart.common.fragment.ProcessingFragment;
import org.deyi.smart.common.utils.LogUtil;
import org.deyi.smart.common.utils.SettingUtil;

import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends AppCompatActivity {

    public static final String TAG_PROCESSING = "TAG_PROCESSING";
    private static Logger logger = LogUtil.getLogger(BaseActivity.class);
    protected final Handler mHandler = new Handler();
    protected final Thread.UncaughtExceptionHandler mExceptionHandle = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            logger.error("uncaught exception thread:[" + String.valueOf(thread.getName()) + "]\t" + " Error:" + ex.getMessage());
            logger.error(String.valueOf(thread.getName()), ex);
            Intent intent = new Intent(getApplicationContext(), SendErrorLogActivity.class);
            startActivity(intent);
            System.exit(0);
        }
    };
    protected int mOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
    private ProcessingFragment mProcessingFragment = null;
    private MediaPlayer mPlayer = new MediaPlayer();
    private Vibrator mVibrator = null;

    private boolean getVibrator() {
        if (mVibrator == null) {
            mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        }
        return mVibrator.hasVibrator();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(mExceptionHandle);

        mOrientation = getScreenOrientation();
        if (mOrientation != (int) getRequestedOrientation()) {
            if (mOrientation == (int) ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }
        logger.debug("[" + getClass().getName() + "]:" + "onCreate!");
    }

    @Override
    protected void onResume() {
        logger.debug("[" + getClass().getName() + "]:" + "onResume!");
        super.onResume();
    }

    @Override
    protected void onPause() {
        logger.debug("[" + getClass().getName() + "]:" + "onPause!");
        super.onPause();
    }

    @Override
    protected void onStop() {
        logger.debug("[" + getClass().getName() + "]:" + "onStop!");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        logger.debug("[" + getClass().getName() + "]:" + "onRestart!");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        logger.debug("[" + getClass().getName() + "]:" + "onDestroy!");

        cancelVibrate();

        relaeseSound();

        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(mOrientation);
    }

    /**
     * 画面の向き
     *
     * @return 画面の向きを取得
     */
    protected final int getScreenOrientation() {
        int orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        String setting = readSetting(Consts.SETTING_KEY_ORIENTATION);
        if ("1".equals(setting)) {
            orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        }

        return orientation;
    }

    public final synchronized void showProcessing() {
        if (mProcessingFragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.remove(mProcessingFragment);
            ft.commitAllowingStateLoss();
        }
        mProcessingFragment = new ProcessingFragment();
        mProcessingFragment.setCancelable(false);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(mProcessingFragment, TAG_PROCESSING);
        ft.commitAllowingStateLoss();

    }

    public final synchronized void closeProcessing() {
        if (mProcessingFragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.remove(mProcessingFragment);
            ft.commitAllowingStateLoss();
            mProcessingFragment = null;
        }
    }

    public final void saveSetting(final String key, final String value) {
        SettingUtil.saveSetting(this, key, value);
    }

    public final String readSetting(final String key) {
        String value = "";
        value = SettingUtil.readSetting(this, key);
        return value;
    }

    protected final void delayExecute(final long millisecond, final Runnable runnable) {
        mHandler.removeCallbacks(runnable);
        mHandler.postDelayed(runnable, millisecond);
    }

    protected final void executeOnUI(final Runnable runnable) {
        mHandler.removeCallbacks(runnable);
        mHandler.post(runnable);
    }

    protected final void vibrate(long span) {
        long milliseconds = span;
        if (milliseconds <= 0) {
            return;
        }
        if (getVibrator()) {
            mVibrator.vibrate(milliseconds);
        }
    }

    protected final void vibrate() {
        vibrate(20);
    }

    /**
     * 振動
     *
     * @param spans  時間
     * @param repeat 繰り返す回数
     */
    protected void repeatVibrate(long[] spans, int repeat) {
        if (spans == null || spans.length <= 0) {
            return;
        }
        if (getVibrator()) {
            mVibrator.vibrate(spans, repeat);
        }
    }

    protected void cancelVibrate() {
        if (getVibrator()) {
            mVibrator.cancel();
        }
    }

    /**
     * 電話が来たとき、音声流し
     */
    protected final void playSound() {
        try {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
                mPlayer.release();
                mPlayer = new MediaPlayer();
            }
            AssetFileDescriptor descriptor = getAssets().openFd("calling.wav");
            mPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mPlayer.prepare();
            mPlayer.setVolume(1f, 1f);
            mPlayer.setLooping(false);
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            mPlayer.start();

        } catch (Exception e) {
        }
    }

    /**
     * 音声停止
     */
    protected final void stopSound() {
        try {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
                mPlayer.release();
                mPlayer = new MediaPlayer();
            }
        } catch (Exception e) {
        }
    }

    /**
     * 音声リソース開放
     */
    protected final void relaeseSound() {
        try {

            if (mPlayer != null && mPlayer.isPlaying()) {
                mPlayer.stop();
            }
            if (mPlayer != null) {
                mPlayer.release();
                mPlayer = null;
            }

        } catch (Exception e) {
        }
    }

    /**
     * 選択用ダイアログ
     *
     * @param items    リスト用アイテム
     * @param title    タイトル
     * @param listener コールバック用リスナー
     */
    protected final AlertDialog showSelectDialog(final String title, final List<String> items, final OnSelectedListener listener) {
        int defaultItem = 0;
        final List<Integer> checkedItems = new ArrayList<>();
        checkedItems.add(defaultItem);
        return new AlertDialog.Builder(this)
                .setTitle(title)
                .setSingleChoiceItems(items.toArray(new String[0]), defaultItem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkedItems.clear();
                        checkedItems.add(which);
                    }
                })
                .setCancelable(false)
                .setNegativeButton(R.string.BUTTON_TEXT_OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!checkedItems.isEmpty()) {
                            listener.onSelected(items.get(checkedItems.get(0)), checkedItems.get(0), false);
                        }
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.BUTTON_TEXT_CANCEL, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onSelected(null, -1, true);
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /**
     * 確認ダイアログ
     *
     * @param title
     * @param message
     * @param listener
     */
    public final AlertDialog showConfirmDialog(final String title, final String message, final DialogInterface.OnClickListener listener) {
        return new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.BUTTON_TEXT_NO, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.BUTTON_TEXT_YES, listener)
                .show();
    }

    /**
     * 確認ダイアログ
     *
     * @param title
     * @param message
     * @param okListener
     * @param cacnelListener
     */
    public final AlertDialog showConfirmDialog(final String title, final String message, final DialogInterface.OnClickListener okListener, final DialogInterface.OnClickListener cacnelListener) {
        return new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.BUTTON_TEXT_NO, cacnelListener)
                .setNegativeButton(R.string.BUTTON_TEXT_YES, okListener)
                .show();
    }

    /**
     * 確認ダイアログ
     *
     * @param title
     * @param message
     */
    public final AlertDialog showAlertDialog(final String title, final String message) {

        return new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.BUTTON_TEXT_OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /**
     * 確認ダイアログ
     *
     * @param title
     * @param message
     */
    public final AlertDialog showAlertDialog(final String title, final String message, final DialogInterface.OnClickListener listener) {

        return new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.BUTTON_TEXT_OK, listener)
                .show();
    }

    /**
     * 選択後のコールバック
     */
    protected interface OnSelectedListener {
        void onSelected(String item, int index, Boolean isCanceled);
    }

}
