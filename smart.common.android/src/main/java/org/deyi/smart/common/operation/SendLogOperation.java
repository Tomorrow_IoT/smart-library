package org.deyi.smart.common.operation;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Button;

import org.deyi.smart.common.BuildConfig;
import org.deyi.smart.common.R;
import org.deyi.smart.common.activity.BaseActivity;
import org.deyi.smart.common.common.ExtensionFilter;
import org.deyi.smart.common.common.Logger;
import org.deyi.smart.common.model.ApiResult;
import org.deyi.smart.common.model.rest.Request;
import org.deyi.smart.common.model.rest.SendLogModel;
import org.deyi.smart.common.utils.LogUtil;
import org.deyi.smart.common.utils.Utils;
import org.deyi.smart.common.utils.WebUtil;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class SendLogOperation extends AsyncTask<String, Void, ApiResult<Void>> {

    private static Logger logger = LogUtil.getLogger(SendLogOperation.class);
    private final Button button;
    private final BaseActivity activity;
    private final DialogInterface.OnClickListener onClickListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    button.setEnabled(true);
                }
            };

    public SendLogOperation(Button button, BaseActivity activity) {
        this.button = button;
        this.activity = activity;
    }

    @Override
    protected ApiResult<Void> doInBackground(String... params) {

        logger.debug("SendLog api start");

        SendLogModel model = new SendLogModel();
        StringBuilder sb = new StringBuilder();
        sb.append(MessageFormat.format("Application Version {0}\n\n", BuildConfig.VERSION_NAME));
        sb.append("Setting: s\n");
        sb.append(MessageFormat.format("ssAndroid Versions：{0}\n", Build.VERSION.RELEASE));
        sb.append(MessageFormat.format("Terminal ：{0}\n Model：{1}\n Makers：{2}\n", Build.DEVICE, Build.MODEL, Build.MANUFACTURER));

        String mailAddress = "dy0263@gmail.com";

        if (params != null && params.length > 0 && !Utils.isNullOrEmpty(params[0])) {
            mailAddress = params[0];
        }

        model.title = "Send Error Logs";
        model.body = sb.toString();
        model.toAddress = mailAddress;
        model.extensionName = "zip";

        LogUtil.stopLog();

        final File folder = activity.getFilesDir();
        OutputStream os = null;
        ZipOutputStream zos = null;
        InputStream is = null;
        File zipFile = new File(activity.getFilesDir() + "/" + "log.zip");
        byte[] bytes = new byte[1024];
        int len = 0;
        try {
            if (zipFile.exists()) {
                zipFile.delete();
            }
            os = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(new BufferedOutputStream(os));
            File[] files = folder.listFiles(new ExtensionFilter(".log"));
            if (files != null && files.length > 0) {
                for (File file : files) {
                    if (file != null && file.exists()) {
                        String filename = file.getName();
                        ZipEntry entry = new ZipEntry(filename);
                        entry.setSize(file.length());
                        entry.setTime(file.lastModified());
                        zos.putNextEntry(entry);
                        is = new FileInputStream(file);
                        while ((len = is.read(bytes)) > 0) {
                            zos.write(bytes, 0, len);
                        }
                        zos.closeEntry();
                        is.close();
                        is = null;
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            logger.error(ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (zos != null) {
                try {
                    zos.close();
                    zos = null;
                } catch (Exception ex) {
                }
            }
            if (is != null) {
                try {
                    is.close();
                    is = null;
                } catch (Exception ex) {
                }
            }
            if (os != null) {
                try {
                    os.close();
                    os = null;
                } catch (Exception ex) {
                }
            }
        }

        LogUtil.startLog(activity.getApplicationContext());

        if (zipFile.exists()) {
            ByteArrayOutputStream buffer = null;
            try {
                buffer = new ByteArrayOutputStream();
                is = new FileInputStream(zipFile);
                while ((len = is.read(bytes)) > 0) {
                    buffer.write(bytes, 0, len);
                }
                is.close();
                is = null;
                model.attachFile = buffer.toByteArray();
            } catch (FileNotFoundException ex) {
                logger.error(ex.getMessage(), ex);
            } catch (IOException ex) {
                logger.error(ex.getMessage(), ex);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                        is = null;
                    } catch (Exception ex) {
                    }
                }
                if (buffer != null) {
                    try {
                        buffer.close();
                        buffer = null;
                    } catch (Exception ex) {
                    }
                }
            }
            zipFile.delete();
        }

        Request<SendLogModel> request = new Request<>(model);
        ApiResult<Void> result = WebUtil.sendLog(request, null,logger);

        return result;
    }

    @Override
    protected void onPostExecute(ApiResult<Void> result) {

        activity.closeProcessing();

        logger.debug("sendLog api end result:" + result.result);

        if (!result.result) {
            new AlertDialog.Builder(activity)
                    .setTitle(R.string.TITLE_ERROR_COMMUNICATION)
                    .setMessage(R.string.TEXT_ERROR_COMMUNICATION)
                    .setPositiveButton(R.string.BUTTON_TEXT_OK, onClickListener)
                    .show();
            return;
        }

        new AlertDialog.Builder(activity)
                .setTitle(R.string.TITLE_INFO)
                .setMessage(R.string.TEXT_SEND_COMPLECTED)
                .setPositiveButton(R.string.BUTTON_TEXT_OK, onClickListener)
                .show();
    }

    @Override
    protected void onPreExecute() {
        // 処理中のメッセージを表示
        activity.showProcessing();
    }

    @Override
    protected void onProgressUpdate(Void... values) {

    }
}
