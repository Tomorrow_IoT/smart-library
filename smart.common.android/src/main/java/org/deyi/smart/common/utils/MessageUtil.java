package org.deyi.smart.common.utils;

import android.content.Context;

import org.deyi.smart.common.common.Logger;
import org.deyi.smart.common.common.MessageConsts;
import org.deyi.smart.common.common.MessageConverter;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

public class MessageUtil implements MessageConsts {

    private static Logger logger = LogUtil.getLogger(MessageUtil.class);
    private Context context;

    private MessageUtil() {
    }

    public MessageUtil(Context context) {
        this();
        this.context = context;
    }

    public static byte[] getIntBytes(int val) {
        ByteBuffer buffer = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(val);
        return buffer.array();
    }

    public static byte[] createMessage(List<byte[]> messages, MessageConverter converter) {

        byte[] buffer = Utils.concat(messages);

        logger.trace("createMessage message[HEX]:" + Utils.bytesToHex(buffer));
        if (converter != null) {
            buffer = converter.to(buffer);
        }

        return buffer;
    }

    public static byte[] createMessage(List<byte[]> messages, int maxLength) {

        byte[] result = createMessage(messages, maxLength, null);

        return result;
    }

    public static byte[] createMessage(List<byte[]> messages, int maxLength, MessageConverter converter) {

        byte[] buffer = Utils.concat(messages);

        byte[] result = new byte[maxLength];
        if (buffer.length <= maxLength) {
            System.arraycopy(buffer, 0, result, 0, buffer.length);
        } else {
            System.arraycopy(buffer, 0, result, 0, maxLength);
        }

        logger.trace("createMessage message[HEX]:" + Utils.bytesToHex(buffer));
        if (converter != null) {
            buffer = converter.to(buffer);
        }

        return result;
    }

}
