package org.deyi.smart.common.operation;

import android.os.AsyncTask;

import org.deyi.smart.common.activity.BaseActivity;
import org.deyi.smart.common.common.ApiCallback;
import org.deyi.smart.common.model.ApiResult;

public class RunnableOperation extends AsyncTask<Void, Void, Boolean> {

    private final Runnable runnable;
    private final BaseActivity activity;
    private final ApiCallback<Void> callback;

    public RunnableOperation(final BaseActivity activity, final Runnable runnable, final ApiCallback<Void> callback) {
        this.activity = activity;
        this.runnable = runnable;
        this.callback = callback;
    }

    @Override
    protected Boolean doInBackground(Void... conditions) {

        if (runnable != null) {
            runnable.run();
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {

        if (result && this.callback != null) {
            ApiResult<Void> data = new ApiResult<Void>(result, null);
            this.callback.onCompleted(data);
        }
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected void onProgressUpdate(Void... values) {

    }
}
