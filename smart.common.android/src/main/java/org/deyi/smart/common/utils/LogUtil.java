package org.deyi.smart.common.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.deyi.smart.common.common.Consts;
import org.deyi.smart.common.common.ExtensionFilter;
import org.deyi.smart.common.common.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.Date;

public class LogUtil {

    public static final String LOGGER_FILE_NAME = "log.{0}.log";

    private static volatile Boolean isEnableDebug = false;
    private static LogWriter logWriter = new LogWriter();

    public static void startLog(Context context) {
        logWriter.startLog(context);
    }

    public static void stopLog() {
        logWriter.stopLog();
    }

    public static void enableDebug(Boolean enableDebug) {
        isEnableDebug = enableDebug;
    }

    public static Logger getLogger(Class clazz) {
        return getLogger(clazz.getName());
    }

    private static Logger getLogger(String name) {
        return new LocalLogger(name);
    }

    private static final void deleteExpiredLog(final Context context) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... unused) {
                try {
                    Date today = Consts.DATE_FORMAT.parse(Consts.DATE_FORMAT.format(new Date()));
                    final int ONE_WEEK = 7 * 24 * 60 * 60 * 1000;
                    final File folder = context.getFilesDir();
                    File[] files = folder.listFiles(new ExtensionFilter(".log"));
                    if (files != null && files.length > 0) {
                        for (File file : files) {
                            if (file != null && file.exists()) {
                                Date dt = new Date(file.lastModified());
                                String fileModified = Consts.DATE_FORMAT.format(dt);
                                dt = Consts.DATE_FORMAT.parse(fileModified);
                                dt = new Date(dt.getTime() + ONE_WEEK);
                                if (dt.before(today)) {
                                    file.delete();
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                }
                return null;
            }
        }.execute();
    }

    public final static void deleteLog(final Context context) {

        LogUtil.stopLog();
        try {
            final File folder = context.getFilesDir();
            File[] files = folder.listFiles(new ExtensionFilter(".log"));
            if (files != null && files.length > 0) {
                for (File file : files) {
                    if (file != null && file.exists()) {
                        file.delete();
                    }
                }
            }
        } catch (Exception ex) {
        }

        LogUtil.startLog(context.getApplicationContext());
    }

    public final static String getLogSize(final Context context) {

        String result = "";
        long size = 0;

        LogUtil.stopLog();

        final File folder = context.getFilesDir();
        File[] files = folder.listFiles(new ExtensionFilter(".log"));
        if (files != null && files.length > 0) {
            for (File file : files) {
                if (file != null && file.exists()) {
                    size += file.length();
                }
            }
        }

        LogUtil.startLog(context.getApplicationContext());

        long mSize = (long) Math.floor(size / (1024 * 1024));
        size = size - (mSize * 1024 * 1024);
        double bSize = (double) Math.floor(size * 1.0d / 1024);
        result = new DecimalFormat("0.0").format(bSize);

        result = result + "Kb";
        if (mSize > 0) {
            result = mSize + "M " + result;
        }

        return result;
    }

    enum LogLevel {
        Trace,
        Debug,
        Info,
        Warn,
        Error
    }

    private static class LocalLogger implements Logger {

        private String name;
        private String simpleName;

        public LocalLogger(String name) {
            this.name = name;
            if (this.name.contains(".")) {
                this.simpleName = this.name.substring(this.name.lastIndexOf(".") + 1);
            } else {
                this.simpleName = this.name;
            }

        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public void trace(String var1) {
            if (!isEnableDebug) {
                return;
            }
            logWriter.writelog(LogLevel.Trace, this.simpleName, var1);
        }

        @Override
        public void trace(String var1, Object var2) {
            if (!isEnableDebug) {
                return;
            }
            logWriter.writelog(LogLevel.Trace, this.simpleName, MessageFormat.format("{0},{1}", var1, var2));
        }

        @Override
        public void trace(String var1, Object var2, Object var3) {
            if (!isEnableDebug) {
                return;
            }
            logWriter.writelog(LogLevel.Trace, this.simpleName, MessageFormat.format("{0},{1},{2}", var1, var2, var3));
        }

        @Override
        public void trace(String var1, Object[] var2) {
            if (!isEnableDebug) {
                return;
            }
            logWriter.writelog(LogLevel.Trace, this.simpleName, MessageFormat.format(var1, var2));
        }

        @Override
        public void trace(String var1, Throwable var2) {
            if (!isEnableDebug) {
                return;
            }
            logWriter.writelog(LogLevel.Trace, this.simpleName, var1);
            logWriter.writelog(LogLevel.Trace, this.simpleName, var2);
        }

        @Override
        public void debug(String var1) {
            logWriter.writelog(LogLevel.Debug, this.simpleName, var1);
        }

        @Override
        public void debug(String var1, Object var2) {
            logWriter.writelog(LogLevel.Debug, this.simpleName, MessageFormat.format("{0},{1}", var1, var2));
        }

        @Override
        public void debug(String var1, Object var2, Object var3) {
            logWriter.writelog(LogLevel.Debug, this.simpleName, MessageFormat.format("{0},{1},{2}", var1, var2, var3));
        }

        @Override
        public void debug(String var1, Object[] var2) {
            logWriter.writelog(LogLevel.Debug, this.simpleName, MessageFormat.format(var1, var2));
        }

        @Override
        public void debug(String var1, Throwable var2) {
            logWriter.writelog(LogLevel.Debug, this.simpleName, var1);
            logWriter.writelog(LogLevel.Debug, this.simpleName, var2);
        }

        @Override
        public void info(String var1) {
            logWriter.writelog(LogLevel.Info, this.simpleName, var1);
        }

        @Override
        public void info(String var1, Object var2) {
            logWriter.writelog(LogLevel.Info, this.simpleName, MessageFormat.format("{0},{1}", var1, var2));
        }

        @Override
        public void info(String var1, Object var2, Object var3) {
            logWriter.writelog(LogLevel.Info, this.simpleName, MessageFormat.format("{0},{1},{2}", var1, var2, var3));
        }

        @Override
        public void info(String var1, Object[] var2) {
            logWriter.writelog(LogLevel.Info, this.simpleName, MessageFormat.format(var1, var2));
        }

        @Override
        public void info(String var1, Throwable var2) {
            logWriter.writelog(LogLevel.Info, this.simpleName, var1);
            logWriter.writelog(LogLevel.Info, this.simpleName, var2);
        }

        @Override
        public void warn(String var1) {
            logWriter.writelog(LogLevel.Warn, this.simpleName, var1);
        }

        @Override
        public void warn(String var1, Object var2) {
            logWriter.writelog(LogLevel.Warn, this.simpleName, MessageFormat.format("{0},{1}", var1, var2));
        }

        @Override
        public void warn(String var1, Object[] var2) {
            logWriter.writelog(LogLevel.Warn, this.simpleName, MessageFormat.format(var1, var2));
        }

        @Override
        public void warn(String var1, Object var2, Object var3) {
            logWriter.writelog(LogLevel.Warn, this.simpleName, MessageFormat.format("{0},{1},{2}", var1, var2, var3));
        }

        @Override
        public void warn(String var1, Throwable var2) {
            logWriter.writelog(LogLevel.Warn, this.simpleName, var1);
            logWriter.writelog(LogLevel.Warn, this.simpleName, var2);
        }

        @Override
        public void error(String var1) {
            logWriter.writelog(LogLevel.Error, this.simpleName, var1);
        }

        @Override
        public void error(String var1, Object var2) {
            logWriter.writelog(LogLevel.Error, this.simpleName, MessageFormat.format("{0},{1}", var1, var2));
        }

        @Override
        public void error(String var1, Object var2, Object var3) {
            logWriter.writelog(LogLevel.Error, this.simpleName, MessageFormat.format("{0},{1},{3}", var1, var2, var3));
        }

        @Override
        public void error(String var1, Object[] var2) {
            logWriter.writelog(LogLevel.Error, this.simpleName, MessageFormat.format(var1, var2));
        }

        @Override
        public void error(String var1, Throwable var2) {
            logWriter.writelog(LogLevel.Error, this.simpleName, var1);
            logWriter.writelog(LogLevel.Error, this.simpleName, var2);
        }
    }

    private static final class LogWriter {

        final String LOG_FORMAT = "{0} {1} {2} {3}";
        BufferedWriter writer = null;
        private Object obj = new Object();
        private String lastLogDate = "";
        private Context context = null;

        public LogWriter() {
        }

        private Boolean getWriter() {

            Boolean status = false;
            String today = Consts.DATE_FORMAT.format(new Date());
            if (!Utils.isNullOrEmpty(lastLogDate) && !lastLogDate.equals(today)) {
                stopLog();
                deleteExpiredLog(this.context);
            }

            if (writer != null) {

                try {
                    writer.newLine();
                    status = true;
                } catch (IOException ex) {
                    if (ex != null && !Utils.isNullOrEmpty(ex.getMessage())) {
                        Log.e("LogWriter", ex.getMessage());
                    }
                }

            } else {

                try {

                    if (this.context == null) {
                        return status;
                    }

                    lastLogDate = today;
                    File logger = new File(this.context.getFilesDir().getPath() + "/" + MessageFormat.format(LOGGER_FILE_NAME, lastLogDate));
                    writer = new BufferedWriter(new FileWriter(logger, true));
                    writer.newLine();
                    status = true;
                } catch (IOException ex) {
                    if (ex != null && !Utils.isNullOrEmpty(ex.getMessage())) {
                        Log.e("LogWriter", ex.getMessage());
                    }
                }
            }

            return status;
        }

        private void log(LogLevel level, String className, String messsage) {

            try {

                String buffer = MessageFormat.format(LOG_FORMAT, Consts.DATETIME_FORMAT.format(new Date()), level.toString(), className, messsage);

                if (context != null) {
                    switch (level) {
                        case Trace:
                            Log.v(className, buffer);
                            break;
                        case Debug:
                            Log.d(className, buffer);
                            break;
                        case Info:
                            Log.i(className, buffer);
                            break;
                        case Warn:
                            Log.w(className, buffer);
                            break;
                        case Error:
                            Log.e(className, buffer);
                            break;
                        default:
                            break;
                    }
                }

                if (!getWriter()) {
                    return;
                }

                writer.write(buffer);

            } catch (IOException ex) {

                if (ex != null && !Utils.isNullOrEmpty(ex.getMessage())) {
                    if (context != null) {
                        Log.e("LogWriter", ex.getMessage());
                    }
                }
            }
        }

        public void writelog(LogLevel level, String className, String messsage) {

            synchronized (obj) {
                log(level, className, messsage);
            }

        }

        public void writelog(LogLevel level, String className, Throwable ex) {

            synchronized (obj) {
                try {
                    String message = "";
                    String buffer = "";
                    StackTraceElement[] stacktraces = null;

                    if (ex != null && !Utils.isNullOrEmpty(ex.getMessage())) {
                        message = ex.getMessage();
                        stacktraces = ex.getStackTrace();
                    }

                    if (!Utils.isNullOrEmpty(message)) {
                        String dt = Consts.DATETIME_FORMAT.format(new Date());
                        buffer = MessageFormat.format(LOG_FORMAT, dt, level.toString(), className, message);
                        if (writer != null) {
                            writer.write(buffer);
                        }

                        if (context != null) {
                            Log.e("LogWriter", buffer);
                        }

                        if (stacktraces != null && stacktraces.length > 0) {
                            for (StackTraceElement stacktrace : stacktraces) {
                                buffer = MessageFormat.format(LOG_FORMAT, dt, level.toString(), className, stacktrace.toString());
                                if (writer != null) {
                                    writer.write(buffer);
                                    writer.flush();
                                }

                                if (context != null) {
                                    Log.e("LogWriter", buffer);
                                }
                            }
                        }
                    }

                } catch (IOException e) {
                    if (e != null && !Utils.isNullOrEmpty(e.getMessage())) {
                        Log.e("LogWriter", e.getMessage());
                    }
                }
            }
        }

        public void startLog(Context context) {
            this.context = context;
            getWriter();
        }

        public void stopLog() {

            synchronized (obj) {
                if (writer != null) {
                    try {
                        writer.flush();
                        writer.close();
                        writer = null;
                    } catch (IOException ex) {
                        if (ex != null && !Utils.isNullOrEmpty(ex.getMessage())) {
                            Log.e("LogWriter", ex.getMessage());
                        }
                    }
                }
            }
        }
    }
}
