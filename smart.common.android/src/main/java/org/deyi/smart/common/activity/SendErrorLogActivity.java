package org.deyi.smart.common.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.deyi.smart.common.R;
import org.deyi.smart.common.common.Logger;
import org.deyi.smart.common.operation.SendLogOperation;
import org.deyi.smart.common.utils.LogUtil;

public class SendErrorLogActivity extends BaseActivity {

    private static Logger logger = LogUtil.getLogger(SendErrorLogActivity.class);
    private Button btnSendLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_send_error_log);
        btnSendLog = (Button) findViewById(R.id.btnSendLog);

        btnSendLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendLogOperation operation = new SendLogOperation(btnSendLog, SendErrorLogActivity.this);
                operation.execute("");
            }
        });
    }

    @Override
    public void onBackPressed() {

        logger.trace("onBackPressed");

        logger.trace("startMain");
        finish();

    }
}
