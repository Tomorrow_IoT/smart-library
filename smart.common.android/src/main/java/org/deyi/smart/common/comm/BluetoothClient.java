package org.deyi.smart.common.comm;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;

import org.deyi.smart.common.common.CommClient;
import org.deyi.smart.common.common.Logger;
import org.deyi.smart.common.utils.LogUtil;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class BluetoothClient extends CommClient {

    private static final Logger logger = LogUtil.getLogger(BluetoothClient.class);
    private static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothSocket socket;

    public BluetoothClient() {
        super(logger);
    }

    public boolean connect(String deviceName, String macAddress) {

        try {
            if (isConnected) {
                disconnect();
            }

            final BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
            if (!adapter.isEnabled()){
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... objects) {
                        adapter.enable();
                        return null;
                    }
                }.execute();
                Thread.sleep(3000);
            }

            Set<BluetoothDevice> devices = adapter.getBondedDevices();
            BluetoothDevice targetDevice = null;

            for (BluetoothDevice device : devices) {
                if (device.getName().equals(deviceName) || device.getAddress().equals(macAddress)) {
                    targetDevice = device;
                    break;
                }
            }

            if (targetDevice != null) {
                socket = targetDevice.createRfcommSocketToServiceRecord(SPP_UUID);
                try{
                    socket.connect();
                }catch (IOException e1){
                    logger.error(e1.getMessage(), e1);
                    try {
                        logger.info("trying fallback...");
                        socket =(BluetoothSocket) targetDevice.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(targetDevice,1);
                        socket.connect();
                    }catch (IOException e2){
                        logger.error(e1.getMessage(), e1);
                    }
                }
                inputStream = socket.getInputStream();
                outputStream = socket.getOutputStream();
                isConnected = true;
            }

        } catch (Exception ex) {

            logger.error(ex.getMessage(), ex);

            try {
                if (socket != null && socket.isConnected()) {
                    socket.close();
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }

            socket = null;
            isConnected = false;
        }

        return isConnected;
    }

    @Override
    public void disconnect() {

        super.disconnect();

        try {
            if (socket != null && socket.isConnected()) {
                socket.close();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        socket = null;
    }
}
