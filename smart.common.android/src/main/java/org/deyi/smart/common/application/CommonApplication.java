package org.deyi.smart.common.application;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;

import org.deyi.smart.common.common.Consts;
import org.deyi.smart.common.common.Logger;
import org.deyi.smart.common.utils.LogUtil;
import org.deyi.smart.common.utils.SettingUtil;

public class CommonApplication extends MultiDexApplication implements Application.ActivityLifecycleCallbacks {

    private static Logger logger = LogUtil.getLogger(CommonApplication.class);

    @Override
    public void onCreate() {
        super.onCreate();

        registerActivityLifecycleCallbacks(this);

        // 設定値を読み込み
        String debugMode = (readSetting(Consts.SETTING_KEY_DEBUG_MODE));
        // Debugモード有効可
        LogUtil.startLog(getApplicationContext());
        LogUtil.enableDebug("1".equals(debugMode));
    }

    public final void saveSetting(final String key, final String value) {

        SettingUtil.saveSetting(this.getApplicationContext(), key, value);
    }

    public final String readSetting(final String key) {

        String value = SettingUtil.readSetting(this.getApplicationContext(), key);

        return value;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
