package org.deyi.smart.common.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class SettingUtil {

    public static final void saveSetting(final Context context, final String key, final String value) {

        SharedPreferences setting = context.getSharedPreferences(context.getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = setting.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static final String readSetting(final Context context, final String key) {

        String value = "";
        SharedPreferences setting = context.getSharedPreferences(context.getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
        if (setting.contains(key)) {
            value = setting.getString(key, "");
        }

        return value;
    }

    public static final String getMacAddress(final Context context) {

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String macAddr = wifiInfo.getMacAddress();

        return macAddr;
    }

}
