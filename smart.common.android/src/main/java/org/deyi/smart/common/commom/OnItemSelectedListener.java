package org.deyi.smart.common.commom;

public interface OnItemSelectedListener<T> {
    void onSelected(T t);
}
