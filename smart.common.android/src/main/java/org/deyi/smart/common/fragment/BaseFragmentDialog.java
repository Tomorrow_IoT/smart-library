package org.deyi.smart.common.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import org.deyi.smart.common.R;

public class BaseFragmentDialog extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FragmentDialog);
    }

    @Override
    public void onResume() {
        super.onResume();

        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog,
                                 int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    getActivity().getSupportFragmentManager().beginTransaction().remove(BaseFragmentDialog.this).commitAllowingStateLoss();
                    return true;
                } else return false;
            }
        });
    }

    public interface OnCloseListener {
        void onClose();

    }
}
